# Serialization Utilities

## About
This library contains various easy to use extension methods for serializing objects.

## Features

 * Serialize DataContract to byte array.
 * Serialize DataContract to XML string.
 * Serialize Protobuf to Protobuf string.
 * De-serialize byte array to data contract.
 * De-serialize XML string to data contract.
 * De-serialize Protobuf string to protobuf object.


## Installation
Please install via [NuGet](https://www.nuget.org).

```
install-package Rsft.Util.Serialization
```

## Example Usage
The following example demonstrates how to serialize a data contract object to a string containing XML.

```c#

using System;
using System.Diagnostics;

using NUnit.Framework;

using Rsft.Util.Serialization.Tests.TestObjects;

[Test]
public static void SerializeToString_WhenUsingMessageEnvelopeWithoutCompression_ExpectString()
{
	// arrange
	const int MessageType = 500;

	var messageEnvelope = new MessageEnvelope(MessageType);

	messageEnvelope.Payload = new MessageContent { Id = 1, Value = @"Test" };

	// act
	var serializedToString = messageEnvelope.SerializeDataContractToXmlString();

	// assert
	Assert.IsNotNull(serializedToString);
	Console.WriteLine(serializedToString);
	Console.WriteLine(@"String length: {0} characters", serializedToString.Length);
}

```
