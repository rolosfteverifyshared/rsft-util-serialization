﻿// <copyright file="DataContractSerializationIntegrationTests.cs" company="Rolosoft Ltd">
// (c) 2017, Rolosoft Ltd
// </copyright>

// Copyright 2014 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Util.Serialization.Tests.Integration
{
    using System.Diagnostics;
    using System.Diagnostics.Contracts;
    using JetBrains.Annotations;
    using TestObjects;
    using Xunit;
    using Xunit.Abstractions;

    /// <summary>
    /// The data contract serialization tests.
    /// </summary>
    public class DataContractSerializationIntegrationTests : TestBase
    {
        public DataContractSerializationIntegrationTests([NotNull] ITestOutputHelper outHelper) 
            : base(outHelper)
        {
            Contract.Requires(outHelper != null);
        }

        [Fact]
        public void SerializeAndDeserializeSerializeDataContractToXmlString_Integration_ExpectClassPreservation()
        {
            // arrange
            const int messageType = 500;

            var messageEnvelope =
                new MessageEnvelope(messageType) { Payload = new MessageContent { Id = 1, Value = @"Test" } };

            // act
            var stopwatch = Stopwatch.StartNew();
            var serializeDataContractToXmlString = messageEnvelope.SerializeDataContractToXmlString();
            var deserializeXmlStringToContractClass = serializeDataContractToXmlString.DeserializeXmlStringToContractClass<MessageEnvelope>();
            stopwatch.Stop();

            // assert
            Assert.NotNull(deserializeXmlStringToContractClass);
            Assert.IsType<MessageEnvelope>(deserializeXmlStringToContractClass);
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }

        [Fact]
        public void SerializeAndDeserializeSerializeDataContractClassToByteArray_Integration_ExpectClassPreservation()
        {
            // arrange
            const int messageType = 500;

            var messageEnvelope =
                new MessageEnvelope(messageType) { Payload = new MessageContent { Id = 1, Value = @"Test"}};

            // act
            var stopwatch = Stopwatch.StartNew();
            var serializeDataContractClassToByteArray = messageEnvelope.SerializeDataContractClassToByteArray();
            var deserializeBytesToDataContractClass = serializeDataContractClassToByteArray.DeserializeBytesToDataContractClass<MessageEnvelope>();
            stopwatch.Stop();

            // assert
            Assert.NotNull(deserializeBytesToDataContractClass);
            Assert.IsType<MessageEnvelope>(deserializeBytesToDataContractClass);
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }
    }
}