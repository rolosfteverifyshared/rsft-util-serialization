﻿namespace Rsft.Util.Serialization.Tests
{
    using System;

    using JetBrains.Annotations;

    using Xunit.Abstractions;

    public abstract class TestBase
    {
        protected TestBase([NotNull] ITestOutputHelper outHelper)
        {
            this.OutHelper = outHelper;
        }

        /// <summary>
        /// Gets the out helper.
        /// </summary>
        /// <value>
        /// The out helper.
        /// </value>
        protected ITestOutputHelper OutHelper { get; }

        /// <summary>
        /// The write time elapsed.
        /// </summary>
        /// <param name="timerElapsed">
        /// The timer elapsed.
        /// </param>
        protected void WriteTimeElapsed(long timerElapsed)
        {
            this.OutHelper.WriteLine($"Elapsed timer: {timerElapsed}ms");
        }

        /// <summary>
        /// The write time elapsed.
        /// </summary>
        /// <param name="timerElapsed">
        /// The timer elapsed.
        /// </param>
        protected void WriteTimeElapsed(TimeSpan timerElapsed)
        {
            this.OutHelper.WriteLine($"Elapsed timer: {timerElapsed}");
        }
    }
}
