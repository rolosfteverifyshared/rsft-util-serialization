﻿// Copyright 2014 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Rsft.Util.Serialization.Tests.Unit
{
    using System.Diagnostics;
    using System.Diagnostics.Contracts;
    using JetBrains.Annotations;
    using TestObjects;
    using Xunit;
    using Xunit.Abstractions;

    /// <summary>
    /// The data contract serialization unit tests.
    /// </summary>
    public class DataContractSerializationUnitTests : TestBase
    {
        public DataContractSerializationUnitTests([NotNull] ITestOutputHelper outHelper)
            : base(outHelper)
        {
            Contract.Requires(outHelper != null);
        }

        [Fact]
        public void SerializeToString_WhenUsingMessageEnvelopeWithoutCompression_ExpectString()
        {
            // arrange
            const int messageType = 500;

            var messageEnvelope =
                new MessageEnvelope(messageType) {Payload = new MessageContent {Id = 1, Value = @"Test"} };

            // act
            var stopwatch = Stopwatch.StartNew();
            var serializedToString = messageEnvelope.SerializeDataContractToXmlString();
            stopwatch.Stop();

            // assert
            Assert.NotNull(serializedToString);
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
            this.OutHelper.WriteLine(serializedToString);
            this.OutHelper.WriteLine(@"String length: {0} characters", serializedToString.Length);
        }

        [Fact]
        public void DeserializeBytesToDataContractClass_WhenUsingMessageEnvelopeWithoutCompression_ExpectClass()
        {
            // arrange
            const string serialized = "<MyLib xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://myspace.com\"><MessageId>0ab5b1ea-31c0-486e-a447-e40108dd9d5e</MessageId><MessageType>500</MessageType><Payload xmlns:d2p1=\"http://schemas.datacontract.org/2004/07/Rsft.Util.Serialization.Tests.TestObjects\" i:type=\"d2p1:MessageContent\"><d2p1:Id>1</d2p1:Id><d2p1:Value>Test</d2p1:Value></Payload></MyLib>";

            // act
            var stopwatch = Stopwatch.StartNew();
            var envelope = serialized.DeserializeXmlStringToContractClass<MessageEnvelope>();
            stopwatch.Stop();

            // assert
            Assert.NotNull(envelope);
            Assert.IsType<MessageEnvelope>(envelope);
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }
    }
}