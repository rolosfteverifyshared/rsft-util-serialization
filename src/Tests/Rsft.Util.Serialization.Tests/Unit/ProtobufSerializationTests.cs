﻿// <copyright file="ProtobufSerializationTests.cs" company="Rolosoft Ltd">
// (c) 2017, Rolosoft Ltd
// </copyright>

// Copyright 2014 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Util.Serialization.Tests.Unit
{
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Text;
    using JetBrains.Annotations;
    using TestObjects;
    using Xunit;
    using Xunit.Abstractions;

    public class ProtobufSerializationTests : TestBase
    {
        public ProtobufSerializationTests([NotNull] ITestOutputHelper outHelper)
            : base(outHelper)
        {
            Contract.Requires(outHelper != null);
        }

        [Fact]
        public void SerializeStringContent_ExpectLength()
        {
            // Arrange
            const string testString = @"TestData";
            var bytes = Encoding.UTF8.GetBytes(testString);

            // Act
            var serialize = bytes.SerializeToProtobuf();

            // Assert
            Assert.NotNull(serialize);
            Assert.True(serialize.Length == 10);
        }

        [Fact]
        public void DeserializeStringContent_ExpectString()
        {
            // Arrange
            var list = new List<byte> { 10, 8, 84, 101, 115, 116, 68, 97, 116, 97 };

            byte[] bytes = list.ToArray();

            // Act
            var deserialize = bytes.DeserializeFromProtobuf<string>();

            // Assert
            Assert.NotNull(deserialize);
            Assert.Matches("TestData", deserialize);
        }

        [Fact]
        public void SerializeMessageContent_ExpectLength()
        {
            // Arrange
            var messageContent = new MessageContent { Id = 1, Value = @"test" };

            // Act
            var serialize = messageContent.SerializeToProtobuf();

            // Assert
            Assert.NotNull(serialize);
            Assert.True(serialize.Length == 8);
        }

        [Fact]
        public void DeserializeMessageContent_ExpectObject()
        {
            // Arrange
            var list = new List<byte> { 8, 1, 18, 4, 116, 101, 115, 116 };

            var array = list.ToArray();

            // Act
            var messageContent = array.DeserializeFromProtobuf<MessageContent>();

            // Assert
            Assert.NotNull(messageContent);
            Assert.IsType<MessageContent>(messageContent);
            Assert.True(messageContent.Id == 1);
            Assert.Matches(@"test", messageContent.Value);
        }

        [Fact]
        public void SerializeToProtobufString_ExpectString()
        {
            // Arrange
            var messageContent = new MessageContent { Id = 1, Value = @"test" };

            // Act
            var serializeToProtobufString = messageContent.SerializeToProtobufString();

            // Assert
            const string expected = @"CAESBHRlc3Q=";

            Assert.True(!string.IsNullOrWhiteSpace(serializeToProtobufString));
            Assert.Matches(expected, serializeToProtobufString);
        }

        [Fact]
        public void DeserializeFromProtobufString_ExpectObj()
        {
            // Arrange
            const string ser = @"CAESBHRlc3Q=";

            // Act
            var deserializeFromProtobufString = ser.DeserializeFromProtobufString<MessageContent>();

            // Assert
            Assert.True(deserializeFromProtobufString != null);
            Assert.True(deserializeFromProtobufString.Id == 1);
            Assert.True(deserializeFromProtobufString.Value == @"test");
        }
    }
}