﻿// Copyright 2014 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Rsft.Util.Serialization.Tests.Unit.Helpers
{
    using System;

    using JetBrains.Annotations;

    using Rsft.Util.Serialization.Helpers;

    using Xunit;
    using Xunit.Abstractions;

    /// <summary>
    /// Safe ASCII tests.
    /// </summary>
    /// <seealso cref="TestBase" />
    public class SafeAsciiTests : TestBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SafeAsciiTests"/> class.
        /// </summary>
        /// <param name="outHelper">The out helper.</param>
        public SafeAsciiTests([NotNull] ITestOutputHelper outHelper)
            : base(outHelper)
        {
        }

        /// <summary>
        /// Strips the unwanted control characters from string when file separator expect clean.
        /// </summary>
        [Fact]
        public void StripUnwantedControlCharactersFromStringWhenFileSeparatorExpectClean()
        {
            // Arrange
            var testChars = new[] { 't', 'e', 's', 't', Convert.ToChar(28) };
            var test = new string(testChars);

            // Act
            var stripUnwantedControlCharactersFromString = test.StripIllegalXmlCharactersFromString();

            // Assert
            Assert.Equal("test", stripUnwantedControlCharactersFromString, StringComparer.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Strips the unwanted control characters from string when no unwanted chars expect no clean.
        /// </summary>
        [Fact]
        public void StripUnwantedControlCharactersFromStringWhenNoUnwantedCharsExpectNoClean()
        {
            // Arrange
            var testChars = new[] { 't', 'e', 's', 't' };
            var test = new string(testChars);

            // Act
            var stripUnwantedControlCharactersFromString = test.StripIllegalXmlCharactersFromString();

            // Assert
            Assert.Equal("test", stripUnwantedControlCharactersFromString, StringComparer.InvariantCultureIgnoreCase);
        }
    }
}