﻿// Copyright 2014 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Rsft.Util.Serialization.Tests.TestObjects
{
    using System;
    using System.Runtime.Serialization;
    using ProtoBuf;

    /// <summary>
    /// The message envelope.
    /// </summary>
    [KnownType(typeof(MessageContent))]
    [ProtoContract]
    [DataContract(Namespace = @"http://myspace.com", Name = @"MyLib")]
    public sealed class MessageEnvelope
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageEnvelope"/> class.
        /// </summary>
        /// <param name="messageType">
        /// The message type.
        /// </param>
        public MessageEnvelope(int messageType)
        {
            this.MessageType = messageType;
            this.MessageId = Guid.NewGuid().ToString();
        }

        /// <summary>
        ///     Gets the message id.
        /// </summary>
        [ProtoMember(1)]
        [DataMember(IsRequired = true)]
        public string MessageId { get; private set; }

        /// <summary>
        ///     Gets the message type.
        /// </summary>
        [ProtoMember(2)]
        [DataMember(IsRequired = true)]
        public int MessageType { get; private set; }

        /// <summary>
        ///     Gets or sets the payload.
        /// </summary>
        [ProtoMember(3)]
        [DataMember]
        public object Payload { get; set; }
    }
}