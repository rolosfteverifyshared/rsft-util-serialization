﻿// <copyright file="ProtobufSerialization.cs" company="Rolosoft Ltd">
// (c) 2017, Rolosoft Ltd
// </copyright>

// Copyright 2017 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Rsft.Util.Serialization
{
    using System;
    using System.IO;

    using JetBrains.Annotations;

    using ProtoBuf;

    /// <summary>
    /// Protobuf Serialization.
    /// </summary>
    public static class ProtobufSerialization
    {
        /// <summary>
        /// Deserializes the specified data.
        /// </summary>
        /// <typeparam name="T">Type of object to deserialize.</typeparam>
        /// <param name="data">The data.</param>
        /// <returns>The <see cref="T"/>.</returns>
        /// <exception cref="ArgumentNullException">data</exception>
        [NotNull]
        public static T DeserializeFromProtobuf<T>([NotNull] this byte[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            using (var ms = new MemoryStream(data))
            {
                var deserialize = Serializer.Deserialize<T>(ms);

                return deserialize;
            }
        }

        /// <summary>
        /// Serializes the specified object.
        /// </summary>
        /// <typeparam name="T">Type of object to serialize.</typeparam>
        /// <param name="obj">The object.</param>
        /// <returns>The <see cref="byte[]"/></returns>
        /// <exception cref="ArgumentNullException">obj</exception>
        [NotNull]
        public static byte[] SerializeToProtobuf<T>([NotNull] this T obj)
            where T : class
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            using (var ms = new MemoryStream())
            {
                Serializer.Serialize(ms, obj);

                ms.Position = 0;
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Serializes to protobuf string.
        /// </summary>
        /// <typeparam name="T">Type of object to serialize.</typeparam>
        /// <param name="obj">The object.</param>
        /// <returns>Base 64 encoded string protobuf.</returns>
        /// <exception cref="ArgumentNullException">obj</exception>
        [NotNull]
        public static string SerializeToProtobufString<T>([NotNull] this T obj)
            where T : class
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            string s;

            using (var ms = new MemoryStream())
            {
                Serializer.Serialize(ms, obj);

                ms.Position = 0;

                var tryGetBuffer = ms.TryGetBuffer(out var buff);

                s = tryGetBuffer ? Convert.ToBase64String(buff.Array, 0, (int)ms.Length) : string.Empty;
            }

            return s;
        }

        /// <summary>
        /// Deserializes the specified data string.
        /// </summary>
        /// <typeparam name="T">Type of object to deserialize.</typeparam>
        /// <param name="data">The data.</param>
        /// <returns>The <see cref="T"/>.</returns>
        /// <exception cref="ArgumentNullException">data is null</exception>
        [NotNull]
        public static T DeserializeFromProtobufString<T>([NotNull] this string data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var fromBase64String = Convert.FromBase64String(data);

            using (var ms = new MemoryStream(fromBase64String))
            {
                var deserialize = Serializer.Deserialize<T>(ms);

                return deserialize;
            }
        }
    }
}