﻿// <copyright file="SafeAscii.cs" company="Rolosoft Ltd">
// (c) 2019, Rolosoft Ltd
// </copyright>

// Copyright 2017 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Rsft.Util.Serialization.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using JetBrains.Annotations;

    /// <summary>
    /// Safe ASCII helpers.
    /// </summary>
    public static class SafeAscii
    {
        /// <summary>
        /// The unwanted chars.
        /// </summary>
        [NotNull]
        private static readonly List<char> UnwantedChars
            = new List<char>
                  {
                      Convert.ToChar(1), /*Start of heading*/
                      Convert.ToChar(2), /*Start of text*/
                      Convert.ToChar(3), /*End of text*/
                      Convert.ToChar(4), /*End of transmission*/
                      Convert.ToChar(5), /*Inquiry*/
                      Convert.ToChar(6), /*Ack*/
                      Convert.ToChar(7), /*Bell*/
                      Convert.ToChar(8), /*Backspace*/
                      /*Convert.ToChar(9),*/ /*H. tab*/
                      /*Convert.ToChar(10),*/ /*New line*/
                      Convert.ToChar(11), /*V. tab*/
                      Convert.ToChar(12), /*New page form feed*/
                      /*Convert.ToChar(13),*/ /*Carriage return*/
                      Convert.ToChar(14), /*Shift out*/
                      Convert.ToChar(15), /*Shift in*/
                      Convert.ToChar(16), /*Data line escape*/
                      Convert.ToChar(17), /*Dev ctl 1*/
                      Convert.ToChar(18), /*Dev ctl 2*/
                      Convert.ToChar(19), /*Dev ctl 3*/
                      Convert.ToChar(20), /*Dev ctl 4*/
                      Convert.ToChar(21), /*NACK*/
                      Convert.ToChar(22), /*Sync. idle*/
                      Convert.ToChar(23), /*End of transmission block*/
                      Convert.ToChar(24), /*Cancel*/
                      Convert.ToChar(25), /*End of medium*/
                      Convert.ToChar(26), /*Substitute*/
                      Convert.ToChar(27), /*Escape*/
                      Convert.ToChar(28), /*File separator*/
                      Convert.ToChar(29), /*Group separator*/
                      Convert.ToChar(30), /*Record separator*/
                      Convert.ToChar(31), /*Unit separator*/
                  };

        /// <summary>
        /// Strips the control characters from string.
        /// </summary>
        /// <param name="inputValue">The input value.</param>
        /// <returns>The string less any unicode characters.</returns>
        [CanBeNull]
        public static string StripAllControlCharactersFromString([CanBeNull] this string inputValue)
        {
            if (string.IsNullOrEmpty(inputValue))
            {
                return inputValue;
            }

            var rtn = new string(inputValue.Where(c => c <= sbyte.MaxValue && c > 31).ToArray());

            return rtn;
        }

        /// <summary>
        /// Strips illegal XML characters from string.
        /// </summary>
        /// <param name="inputValue">The input value.</param>
        /// <returns>The string less any unwanted control characters.</returns>
        [CanBeNull]
        public static string StripIllegalXmlCharactersFromString([CanBeNull] this string inputValue)
        {
            if (string.IsNullOrEmpty(inputValue))
            {
                return inputValue;
            }

            var enumerable = inputValue.Where(r => !UnwantedChars.Contains(r) && r <= sbyte.MaxValue).ToArray();

            var rtn = new string(enumerable);

            return rtn;
        }
    }
}