﻿// <copyright file="AssemblyInfo.cs" company="Rolosoft Ltd">
// (c) 2019, Rolosoft Ltd
// </copyright>

// Copyright 2017 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Rsft.Util.Serialization.Tests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100EDF23547554D988F4B38E4A931371E1FEEF3166999EEE241F45ADB5B29C2AB97B648AB111152F0FB0C2122315BBC1BD885DACD2B0CAD9267CC7E72CB6B0E63455944E133BEB083132D66BA1A822B0CB83DAAD74093323637F6E9895ADFEFA992E7A9834C8F274BDA6E6B3700D9CAD78374447E8FC03F574756DF1BFE6D0B68AC")]