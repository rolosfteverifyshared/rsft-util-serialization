﻿// <copyright file="DataContractSerialization.cs" company="Rolosoft Ltd">
// (c) 2017, Rolosoft Ltd
// </copyright>

// Copyright 2017 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Util.Serialization
{
    using System;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Xml;

    using JetBrains.Annotations;

    /// <summary>
    ///     The data contract serialization.
    /// </summary>
    public static class DataContractSerialization
    {
        /// <summary>
        /// Deserializes bytes to data contract class.
        /// </summary>
        /// <typeparam name="T">
        /// Type of data contract class to deserialize.
        /// </typeparam>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="T"/> representing the contract class being deserialized.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// data is null
        /// </exception>
        [NotNull]
        public static T DeserializeBytesToDataContractClass<T>([NotNull] this byte[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            using (var ms = new MemoryStream(data))
            {
                var dataContractSerializer = new DataContractSerializer(typeof(T));

                var readObject = dataContractSerializer.ReadObject(ms);

                return (T)readObject;
            }
        }

        /// <summary>
        /// Deserializes the XML string to contract class.
        /// </summary>
        /// <typeparam name="T">
        /// Type of data contract class to deserialize.
        /// </typeparam>
        /// <param name="xmlData">
        /// The XML data.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// @xmlData
        /// </exception>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        [NotNull]
        public static T DeserializeXmlStringToContractClass<T>([NotNull] this string xmlData)
        {
            if (string.IsNullOrWhiteSpace(xmlData))
            {
                throw new ArgumentNullException(nameof(xmlData));
            }

            var serializer = new DataContractSerializer(typeof(T));

            using (var stringReader = new StringReader(xmlData))
            {
                using (var xml = XmlReader.Create(
                    stringReader,
                    new XmlReaderSettings { DtdProcessing = DtdProcessing.Prohibit, IgnoreWhitespace = true }))
                {
                    var obj = (T)serializer.ReadObject(xml);
                    return obj;
                }
            }
        }

        /// <summary>
        /// Serializes data contract class to byte array.
        /// </summary>
        /// <typeparam name="T">
        /// Type of data contract to serialize.
        /// </typeparam>
        /// <param name="obj">
        /// The object.
        /// </param>
        /// <returns>
        /// The <see cref="byte[]"/> array representing the .
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// @obj is null.
        /// </exception>
        [NotNull]
        public static byte[] SerializeDataContractClassToByteArray<T>([NotNull] this T obj)
            where T : class
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            using (var ms = new MemoryStream())
            {
                var dataContractSerializer = new DataContractSerializer(typeof(T));

                dataContractSerializer.WriteObject(ms, obj);

                return ms.ToArray();
            }
        }

        /// <summary>
        /// Serializes the data contract to XML string.
        /// </summary>
        /// <typeparam name="T">
        /// Type of data contract object to serialize to XML string.
        /// </typeparam>
        /// <param name="obj">
        /// The object.
        /// </param>
        /// <returns>
        /// XML string representing the data contract.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// obj is null
        /// </exception>
        [NotNull]
        public static string SerializeDataContractToXmlString<T>([NotNull] this T obj)
            where T : class
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            var sb = new StringBuilder();
            using (var xml = XmlWriter.Create(sb, new XmlWriterSettings()))
            {
                var serializer = new DataContractSerializer(typeof(T));
                serializer.WriteObject(xml, obj);
            }

            return sb.ToString();
        }
    }
}